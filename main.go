package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/robfig/cron"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Website struct { //website
	ID            primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Url           string             `json:"Url" bson:"Url"`
	VisitInterval int                `json:"VisitInterval" bson:"VisitInterval"`
}

type Visit struct { //visit
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	WebsiteID primitive.ObjectID `json:"WebsiteID,omitempty" bson:"WebsiteID,omitempty"`
	Response  string             `json:"Response" bson:"Response"`
	VisitTime string             `json:"VisitTime" bson:"VisitTime"`
	time      time.Time
}

var client *mongo.Client

func main() {
	fmt.Println("Starting the application...")

	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	clientOptions := options.Client().ApplyURI("mongodb+srv://kzyz:QmJmfcIPbWXneAIn@cluster0.t1coz.mongodb.net/test")
	client, _ = mongo.Connect(ctx, clientOptions)

	Cron()

	router := mux.NewRouter()

	router.HandleFunc("/fetchers", WebsiteCreate).Methods("POST")
	router.HandleFunc("/fetchers", GetWebsites).Methods("GET")
	router.HandleFunc("/fetchers/{id}", GetWebsite).Methods("GET")
	router.HandleFunc("/fetchers/{id}", DeleteWebsite).Methods("DELETE")

	router.HandleFunc("/person", CreatePersonEndpoint).Methods("POST")
	router.HandleFunc("/people", GetPeopleEndpoint).Methods("GET")
	router.HandleFunc("/person/{id}", GetPersonEndpoint).Methods("GET")
	http.ListenAndServe(":8080", router)

}

func Cron() { //cron

	//getwebsites

	var websites []Website
	collection := client.Database("gowebchecker").Collection("websites")
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var website Website
		cursor.Decode(&website)
		websites = append(websites, website)
	}

	//getURL

	fmt.Print(websites)
	resp, err := http.Get("https://google.com/")

	fmt.Print(resp)

	// Cron
	c := cron.New()

	for i, v := range websites {

		c.AddFunc("@every "+strconv.Itoa(websites[i].VisitInterval)+"s", func() {

			resp2, err := http.Get(websites[i].Url)

			var visit Visit

			if err == nil {
				visit = Visit{
					WebsiteID: websites[i].ID,
					Response:  "null",
					VisitTime: time.Now().Format("2006-01-02 15:04:05"),
				}

			}

			if resp2 == nil {
				fmt.Printf("error")

				visit = Visit{
					WebsiteID: websites[i].ID,
					Response:  "null",

					VisitTime: time.Now().Format("2006-01-02 15:04:05"),
				}
			} else {
				visit = Visit{
					WebsiteID: websites[i].ID,
					Response:  resp2.Status,
					VisitTime: time.Now().Format("2006-01-02 15:04:05"),
				}

			}

			fmt.Print(v)

			collection := client.Database("gowebchecker").Collection("visits")
			ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
			result, _ := collection.InsertOne(ctx, visit)

			fmt.Print(resp2)
			fmt.Print(result)
		})

	}

	defer c.Start()
}

func StartCronForWebsite(url string) {

	var website Website
	collection := client.Database("gowebchecker").Collection("websites")
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	err := collection.FindOne(ctx, bson.D{{"Url", "https://tezsstonetsda.com.pl"}}).Decode(&website)

	fmt.Print(err)

	c := cron.New()

	c.AddFunc("@every "+strconv.Itoa(website.VisitInterval)+"s", func() {

		resp2, err := http.Get(website.Url)

		var visit Visit

		if err == nil {
			visit = Visit{
				WebsiteID: website.ID,
				Response:  "null",
				VisitTime: time.Now().Format("2006-01-02 15:04:05"),
			}
		}
		if resp2 == nil {
			fmt.Printf("error")
			visit = Visit{
				WebsiteID: website.ID,
				Response:  "null",
				VisitTime: time.Now().Format("2006-01-02 15:04:05"),
			}
		} else {
			visit = Visit{
				WebsiteID: website.ID,
				Response:  resp2.Status,
				VisitTime: time.Now().Format("2006-01-02 15:04:05"),
			}
		}

		collection := client.Database("gowebchecker").Collection("visits")
		ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
		result, _ := collection.InsertOne(ctx, visit)

		fmt.Print(resp2)
		fmt.Print(result)
	})

	defer c.Start()

}

func DeleteWebsite(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	params := mux.Vars(request)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	//var website Website
	collection := client.Database("gowebchecker").Collection("websites")
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)

	deleteResult, _ := collection.DeleteOne(ctx, bson.M{"_id": id})

	json.NewEncoder(response).Encode(deleteResult)

}

func WebsiteCreate(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	var website Website
	_ = json.NewDecoder(request.Body).Decode(&website)
	collection := client.Database("gowebchecker").Collection("websites")
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, _ := collection.InsertOne(ctx, website)

	StartCronForWebsite(website.Url)

	json.NewEncoder(response).Encode(result)
}

func GetWebsite(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	params := mux.Vars(request)
	id, _ := primitive.ObjectIDFromHex(params["id"])
	var website Website
	collection := client.Database("gowebchecker").Collection("websites")
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	err := collection.FindOne(ctx, Person{ID: id}).Decode(&website)
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}

	var visits []Visit
	collection2 := client.Database("gowebchecker").Collection("visits")
	ctx2, _ := context.WithTimeout(context.Background(), 30*time.Second)
	cursor2, err := collection2.Find(ctx2, bson.D{{"WebsiteID", website.ID}})
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor2.Close(ctx2)
	for cursor2.Next(ctx2) {
		var visit Visit
		cursor2.Decode(&visit)
		visits = append(visits, visit)
	}
	if err := cursor2.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(visits)
	//json.NewEncoder(response).Encode(website)
}

func GetWebsites(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("content-type", "application/json")
	var websites []Website
	collection := client.Database("gowebchecker").Collection("websites")
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var website Website
		cursor.Decode(&website)
		websites = append(websites, website)
	}
	if err := cursor.Err(); err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(response).Encode(websites)
}
